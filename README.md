# mCNA : Improving high-resolution copy number variation analysis from next generation sequencing using unique molecular identifiers.


# Description
Software allowing the detection of copy number changes using UMI. The algorithm is composed of four main steps: 
* the construction of UMI count matrices
* the use of control samples to construct a pseudo-reference
* the computation of log-ratios (LR)
* the segmentation
* the statistical inference of abnormal segmented breaks

# Requirements
* R (version >= 3.4.4)
* R-libraries : optparse, ggplot2, reshape2, stringr, PSCBS, mclust, Rsamtools

# Installation
` Download mCNA sources from Gitlab, install R (>= 3.4.4) and use bioClite() and install.packages() for optparse, ggplot2, reshape2, stringr, PSCBS, mclust, Rsamtools`

# Usage

To get help,
`Rscript --vanilla mCNA.R --help`
```
Usage: mCNA.R [options]


Options:
	-b CHARACTER, --bed=CHARACTER
		path to BED file [default= NULL]

	-r CHARACTER, --references=CHARACTER
		references folder path [default= ./references/]

	-t CHARACTER, --tests=CHARACTER
		tumoral samples folder path [default= ./testedFiles/]

	-o CHARACTER, --out=CHARACTER
		output folder path [default= ./out/]

	-a NUMERIC, --alpha=NUMERIC
		p-value threshold [default= 0.05]

	-c NUMERIC, --cell=NUMERIC
		theorical limit percentage of tumor cells [default= 0.2]

	-w LOGICAL, --overWriteRDS=LOGICAL
		erase previous computed pseudo-reference [default= TRUE]

	-d LOGICAL, --disableCentering=LOGICAL
		disable the centering of log-ratios values [default= FALSE]

	-h, --help
		Show this help message and exit
```

# Output
In the specfied output folder, a plot and a CSV file is generated summarizing mCNA results for each tested sample. 



